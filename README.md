# JenkinsWithDocker

## On Container Restart

1. Run this inside host (ssh -l opc 192.168.200.201)

	```
docker exec -u 0 jenkins $(echo "/usr/sbin/groupmod -g $(stat -c "%g" /var/run/docker.sock) docker")
```

2. Restart Jenkins
[http://192.168.200.201:8552/safeRestart](http://192.168.200.201:8552/safeRestart)
